#include<iostream>

using namespace std;

void merge(int a[], int l, int mid, int r) {
    int i, n1 = mid - l + 1, j, n2 = r - mid, L[n1], R[n2], k;
    for (i = 0; i < n1; L[i] = a[l + i], i++);
    for (j = 0; j < n2; R[j] = a[mid + 1 + j], j++);
    i = 0;
    j = 0;
    k = l;
    while (i < n1 && j < n2) {
        if (L[i] <= R[j]) {
            a[k] = L[i];
            i++;
        } else {
            a[k] = R[j];
            j++;
        }
        k++;
    }
    while (i < n1) {
        a[k] = L[i];
        i++;
        k++;
    }
    while (j < n2) {
        a[k] = R[j];
        j++;
        k++;
    }
}

void merge_sort(int a[], int l, int r) {
    if (l >= r) return;
    int mid = l + (r - l) / 2;
    merge_sort(a, l, mid);
    merge_sort(a, mid + 1, r);
    merge(a, l, mid, r);
}

int main() {
    cout << "Lotfan zarfiat array(n) ra vared konid:\n";
    int n;
    cin >> n;
    cout << "Lotfan anasor array ra vared konid:\n";
    int a[n], i, j;
    for (i = 0; i < n; cin >> a[i], i++);
    merge_sort(a, 0, n - 1);
    cout << "Array moratab shode be sorat zir ast:\n";
    for (j = 0; j < n; cout << a[j] << ' ', j++);
    return 0;
}
